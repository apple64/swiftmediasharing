//
//  AppDelegate.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 03/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//

import UIKit
import SwiftInstagram

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        // Show the Instagram login or the InstagramUserProfileTableViewController
        let value = chooseViewControllerToShow()
        
        return value
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: - Vonvinience
    
    // Show the Instagram login or the InstagramUserProfileTableViewController
    private func chooseViewControllerToShow() -> Bool {
        // Grab a reference to the storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "InstagramUserProfileTableViewController") as? InstagramUserProfileTableViewController else {
            return true
        }
        // Initialise the navigation controller
        // Assign the InstagramUserProfileTableViewController as rootViewController
        let navigationController = UINavigationController(rootViewController: vc)
        
        // Check if the user has already sign in
        if UserDefaults.standard.contains(key: ACCESS_TOKEN_STRING), let token = UserDefaults.standard.string(forKey: ACCESS_TOKEN_STRING), token.count > 0,
            UserDefaults.standard.contains(key: USER_ID_STRING), let id = UserDefaults.standard.string(forKey: USER_ID_STRING), id.count > 0,
            Instagram.shared.isAuthenticated == true {
            
            // Inject the access token and the user id to InstagramUserProfileTableViewController
            vc.userID = id
            vc.accessToken = token
            
            // Debug
            printLoginInfo(accessToken: token, userID: id)
        } else {
            let api = Instagram.shared
            
            // Show login view controller for Instagram
            api.login(from: navigationController, success: {
                // Retrieve access token and create user ID
                guard let accessToken = api.retrieveAccessToken() else {
                    return
                }
                guard let userID = accessToken.split(separator: ".").first else {
                    return
                }
                
                // Pass the access token and the user ID
                vc.userID = String(userID)
                vc.accessToken = accessToken
                
                // Save access token and user ID
                UserDefaults.standard.save(accessToken: accessToken, userID: String(userID))
            }, failure: { error in
                print(error.localizedDescription)
            })
        }
        
        // Set navigation controller as the rootViewController
        self.window?.rootViewController = navigationController
        
        // Sets our window up in front
        self.window?.makeKeyAndVisible()
        
        return true
    }

    // MARK: - Debug
    
    private func printLoginInfo(accessToken: String, userID: String) {
        print("------   Loggen in!    ------")
        print("AccessToken \(accessToken)")
        print("User ID \(userID)")
        print("-----------------------------")
    }
}

