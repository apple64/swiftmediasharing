//
//  Constants.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 03/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  App constants

import UIKit

// MARK: - Cell Height

let INSTAGRAM_USER_MAIN_PROFILE_CELL_HEIGHT: CGFloat = 170
let INSTAGRAM_POST_ADDED_HEIGHT: CGFloat = 150

// MARK: - enums

// Kind of data to fetch
enum URLKind {
    case posts
    case profile
}

// MARK: - Strings

let ACCESS_TOKEN_STRING = "accessTokenString"
let USER_ID_STRING = "userIDString"

let INSTAGRAM_POSTS_STRING = "posts"
let INSTAGRAM_FOLLOWERS_STRING = "followers"
let INSTAGRAM_FOLLOWING_STRING = "following"

// MARK: - Images

let NO_IMAGE = UIImage(named: "noImage")
let TABLEVIEW_BACKGROUND = UIImage(named: "tableViewBackground")
let INSTAGRAM_LOGO = UIImage(named: "InstagramLogo")
