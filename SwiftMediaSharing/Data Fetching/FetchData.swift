//
//  FetchData.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 03/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  Fetch data struct for Instagram. It conforms to Gettable protocol.

import Alamofire
import AlamofireObjectMapper

struct FetchUserProfileService: Gettable {
    internal typealias ProfileData = InstagramProfile?
    internal typealias PostsData = InstagramUserPostsList?
    
    func getProfileOf(userID: String?, accessToken: String?, completionHandler: @escaping (Result<InstagramProfile?>) -> Void) {
        
        // Create a URL object in order to read the user's profile.
        let appURLs = AppURLs(userID: nil)
        guard let targetURL = appURLs.getFetchRequestURL(for: userID, accessToken: accessToken, kind: .profile) else {
            return
        }
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        
        Alamofire.request(targetURL).responseObject(keyPath: "data") { (response: DataResponse<InstagramProfile>) in
            switch response.result {
            case .success:
                NetworkActivityIndicatorManager.networkOperationFinished()
                completionHandler(Result.Success(response.result.value))
            case .failure(let error):
                NetworkActivityIndicatorManager.networkOperationFinished()
                completionHandler(Result.Failure(error))
            }
        }
    }
    
    func searchForPostsOf(userID: String?, accessToken: String?, page: UInt?, completionHandler: @escaping (Result<InstagramUserPostsList?>) -> Void) {
        
        // Create a URL object in order to read the user's recent posts.
        let appURLs = AppURLs(userID: nil)
        guard let targetURL = appURLs.getFetchRequestURL(for: userID, accessToken: accessToken, kind: .posts) else {
            return
        }
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        
        Alamofire.request(targetURL).responseObject(keyPath: "") { (response: DataResponse<InstagramUserPostsList>) in
            switch response.result {
            case .success:
                NetworkActivityIndicatorManager.networkOperationFinished()
                completionHandler(Result.Success(response.result.value))
            case .failure(let error):
                NetworkActivityIndicatorManager.networkOperationFinished()
                completionHandler(Result.Failure(error))
            }
        }
    }
}
