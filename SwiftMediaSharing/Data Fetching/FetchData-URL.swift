//
//  FetchData-URL.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 03/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  URL constructor for Instagram JSON requests
//  example -> https://api.instagram.com/v1/users/{user-id}/media/recent/?access_token=ACCESS-TOKEN

import Foundation

struct AppURLs {
    private var userID: String
    
    private let baseFetchRequestString = "https://api.instagram.com/v1/users/"
    private let mediaRecentString = "/media/recent/"
    private let accessTokenString = "?access_token="

    func getFetchRequestURL(for user: String?, accessToken: String?, kind: URLKind) -> URL? {
        guard let token = accessToken else {
            return nil
        }
        
        var fetchRequestString = String()
        
        var uID = ""
        if let id = user {
            uID = id
        } else if userID.count > 0 {
            uID = userID
        } else {
            return nil
        }
        
        switch kind {
        case .profile:
            fetchRequestString = baseFetchRequestString + uID + "/" + accessTokenString + token
            
            let fetchRequestURL = fetchRequestString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            return URL(string: fetchRequestURL)
        case .posts:
            fetchRequestString = baseFetchRequestString + uID + mediaRecentString + accessTokenString + token
            
            let fetchRequestURL = fetchRequestString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            return URL(string: fetchRequestURL)
        }
    }
    
    init(userID: String?) {
        guard let id = userID else {
            self.userID = ""
            return
        }
        self.userID = "\(id)"
    }
}
