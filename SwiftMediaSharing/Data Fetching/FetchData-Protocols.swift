//
//  FetchData-Protocols.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 03/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  Fetch data protocols
//  - getProfileOf      -> Fetches the user's profile data
//  - searchForPostsOf  -> Fetches the recent posts of the user

import Alamofire
import AlamofireObjectMapper

protocol Gettable {
    associatedtype ProfileData
    associatedtype PostsData
    
    func getProfileOf(userID: String?, accessToken: String?, completionHandler: @escaping (Result<ProfileData>) -> Void)
    func searchForPostsOf(userID: String?, accessToken: String?, page: UInt?, completionHandler: @escaping (Result<PostsData>) -> Void)
}
