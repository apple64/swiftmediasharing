//
//  FetchData-Types.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 03/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  Fetch result enum

import Foundation

enum Result<T> {
    case Success(T)
    case Failure(Error)
}
