//
//  InstagramUserPictures.swift
//  SwiftMediaSharing
//
//  Created by Γιώργος Χαλκιαδούδης on 4/7/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
// Instagram post picture object mapper

import ObjectMapper

class InstagramUserPictures: Mappable {
    
    var thumbnail: InstagramUserPostPicture?
    var lowResolution: InstagramUserPostPicture?
    var standardResolution: InstagramUserPostPicture?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        thumbnail             <- map["thumbnail"]
        lowResolution         <- map["low_resolution"]
        standardResolution    <- map["standard_resolution"]
    }
}
