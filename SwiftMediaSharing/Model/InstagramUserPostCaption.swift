//
//  InstagramUserPostCaption.swift
//  SwiftMediaSharing
//
//  Created by Γιώργος Χαλκιαδούδης on 5/7/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
// Instagram post captions object mapper

import ObjectMapper

class InstagramUserPostCaption: Mappable {
    var id: String?
    var text: String?
    var createdTime: String?
    var fromID: String?
    var fromFullName: String?
    var fromProfilePicture: String?
    var fromUsername: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id                 <- map["id"]
        text               <- map["text"]
        
        var createdTimeString: String?
        createdTimeString <- map["created_time"]
        if let createdTime = createdTimeString, let timeInterval = Double(createdTime) {
            let date = Date(timeIntervalSince1970: timeInterval)
            self.createdTime = date.toString(style: .long)
        }
        
        fromID             <- map["from.id"]
        fromFullName       <- map["from.full_name"]
        fromProfilePicture <- map["from.profile_picture"]
        fromUsername       <- map["from.username"]
    }
}
