//
//  InstagramUsersInPhoto.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 04/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
// Instagram post users in photo object mapper

import ObjectMapper

class InstagramUsersInPhoto: Mappable {
    var username: String?
    var position: (x: Int, y: Int)?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        username <- map["user.username"]
        
        var pos = (x: 0, y: 0)
        pos.x <- map["position.x"]
        pos.y <- map["position.y"]
        
        position = pos
    }
}
