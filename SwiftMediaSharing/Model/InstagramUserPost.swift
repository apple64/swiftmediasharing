//
//  InstagramUserPost.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 03/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
// Instagram post object mapper

import ObjectMapper

class InstagramUserPost: Mappable {
    var id: String?
    var user: InstagramUser?
    var images: InstagramUserPictures?
    var createdTime: String?
    var caption: InstagramUserPostCaption?
    var userHasLiked: Bool?
    var likes: Int?
    var tags: String?
    var filter: String?
    var comments: Int?
    var type: String?
    var link: String?
    var location: InstagramUserPostLocation?
    var attribution: String?
    var usersInPhoto: [InstagramUsersInPhoto]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id             <- map["id"]
        user           <- map["user"]
        images         <- map["images"]
        
        var createdTimeString: String?
        createdTimeString <- map["created_time"]
        if let createdTime = createdTimeString, let timeInterval = Double(createdTime) {
            let date = Date(timeIntervalSince1970: timeInterval)
            self.createdTime = date.toString(style: .long)
        }
        
        caption        <- map["caption"]
        userHasLiked   <- map["user_has_liked"]
        likes          <- map["likes.count"]
        tags           <- map["tags"]
        filter         <- map["filter"]
        comments       <- map["comments.count"]
        type           <- map["type"]
        link           <- map["link"]
        location       <- map["location"]
        attribution    <- map["attribution"]
        usersInPhoto   <- map["users_in_photo"]
    }
}
