//
//  InstagramUserPostLocation.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 05/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
// Instagram post location object mapper

import ObjectMapper

class InstagramUserPostLocation: Mappable {
    var id: Int?
    var latitude: Int?
    var longitude: Int?
    var name: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id         <- map["id"]
        latitude   <- map["latitude"]
        longitude  <- map["longitude"]
        name       <- map["name"]
    }
}
