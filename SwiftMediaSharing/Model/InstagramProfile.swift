//
//  InstagramProfile.swift
//  SwiftMediaSharing
//
//  Created by Γιώργος Χαλκιαδούδης on 5/7/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
// Instagram user profile object mapper

import ObjectMapper

class InstagramProfile: Mappable {
    var id: String?
    var username: String?
    var profilePicture: URL?
    var fullName: String?
    var bio: String?
    var website: URL?
    var isBusiness: Bool?
    var media: Int?
    var follows: Int?
    var followedBy: Int?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id              <- map["id"]
        username        <- map["username"]
        
        var urlPictureString: String? = nil
        urlPictureString <- map["profile_picture"]
        if let urlStr = urlPictureString, let url = URL(string: urlStr) {
            self.profilePicture = url
        }

        fullName        <- map["full_name"]
        bio             <- map["bio"]
        
        var urlString: String? = nil
        urlString <- map["website"]
        if let urlStr = urlString, let url = URL(string: urlStr) {
            self.website = url
        }
        
        isBusiness      <- map["is_business"]
        media           <- map["counts.media"]
        follows         <- map["counts.follows"]
        followedBy      <- map["counts.followed_by"]
    }
}
