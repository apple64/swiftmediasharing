//
//  InstagramUserPostsList.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 04/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
// Instagram list of user posts object mapper

import ObjectMapper

class InstagramUserPostsList: Mappable {
    var posts: [InstagramUserPost]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        posts <- map["data"]
    }
}
