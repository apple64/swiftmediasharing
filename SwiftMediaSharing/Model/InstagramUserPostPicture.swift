//
//  InstagramUserPostPicture.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 04/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
// Instagram post image object mapper

import ObjectMapper

class InstagramUserPostPicture: Mappable {
    
    var url: URL?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        var urlString: String? = nil
        urlString <- map["url"]
        if let urlStr = urlString, let url = URL(string: urlStr) {
            self.url = url
        }
    }
    
    static func getImageUrl(imageMap: Map, imageKey: String) -> URL? {
        var images: [InstagramUserPostPicture]?
        images <- imageMap[imageKey]
        
        if let images = images {
            if !images.isEmpty {
                return images[0].url
            }
        }
        return nil
    }
}
