//
//  InstagramUser.swift
//  SwiftMediaSharing
//
//  Created by George Chalkiadoudis on 04/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
// Instagram post user object mapper

import ObjectMapper

class InstagramUser: Mappable {
    var id: String?
    var fullname: String?
    var profilePicture: URL?
    var username: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id             <- map["id"]
        fullname       <- map["full_name"]
        
        var urlString: String? = nil
        urlString <- map["profile_picture"]
        if let urlStr = urlString, let url = URL(string: urlStr) {
            self.profilePicture = url
        }
        
        username       <- map["username"]
    }
}
