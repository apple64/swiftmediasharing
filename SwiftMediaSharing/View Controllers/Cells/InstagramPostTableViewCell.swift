//
//  InstagramPostTableViewCell.swift
//  SwiftMediaSharing
//
//  Created by Γιώργος Χαλκιαδούδης on 5/7/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  User's post custom UITableViewCell

import UIKit
import AlamofireImage

class InstagramPostTableViewCell: UITableViewCell {

    @IBOutlet weak var instagramPostUserImage: RoundedUIImageView!
    @IBOutlet weak var instagramPostUser: UILabel!
    @IBOutlet weak var instagramPostLocation: UILabel!
    @IBOutlet weak var instagramPostImage: UIImageView!
    @IBOutlet weak var instagramPostLikes: UILabel!
    @IBOutlet weak var instagramPostComment: UILabel!
    @IBOutlet weak var instagramPostDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // Initialise the labels
        instagramPostLikes.text = ""
        instagramPostComment.text = ""
        instagramPostDate.text = ""
    }
    
    // MARK: - Convenience
    
    /// Image fetching for the post
    public func setImage(_ url: URL?) {
        if let url = url {
            instagramPostImage.af_setImage(withURL: url, imageTransition: .crossDissolve(0.2), completion: { [weak self] response in
                if let _ = response.result.value {
                    
                } else {
                    self?.instagramPostImage.image = NO_IMAGE
                }
            })
        } else {
            instagramPostImage.image = NO_IMAGE
        }
    }
    
    /// Image fetching for the user who posted
    public func setUserImage(_ url: URL?) {
        if let url = url {
            instagramPostUserImage.af_setImage(withURL: url, imageTransition: .crossDissolve(0.2), completion: { [weak self] response in
                if let _ = response.result.value {
                    
                } else {
                    self?.instagramPostUserImage.image = NO_IMAGE
                }
            })
        } else {
            instagramPostUserImage.image = NO_IMAGE
        }
    }
}
