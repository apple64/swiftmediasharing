//
//  InstagramUserMainProfileTableViewCell.swift
//  SwiftMediaSharing
//
//  Created by Γιώργος Χαλκιαδούδης on 5/7/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  User's profile custom UITableViewCell

import UIKit
import AlamofireImage

// Protocol for responding to the logout button
protocol InstagramUserMainProfileTableViewCellDelegate : class {
    func logoutButtonPressed()
}

class InstagramUserMainProfileTableViewCell: UITableViewCell {
    
    // Logout button delegate variable
    weak var cellDelegate: InstagramUserMainProfileTableViewCellDelegate?

    @IBOutlet weak var instagramUserImage: RoundedUIImageView!
    @IBOutlet weak var instagramPostsCount: UILabel!
    @IBOutlet weak var instagramPostsLabel: UILabel!
    @IBOutlet weak var instagramFollowersCount: UILabel!
    @IBOutlet weak var instagramFollowersLabel: UILabel!
    @IBOutlet weak var instagramFollowingCount: UILabel!
    @IBOutlet weak var instagramFollowingLabel: UILabel!
    @IBOutlet weak var instagramUserFullname: UILabel!
    @IBOutlet weak var instagramUserWebsite: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // Initialise the labels
        instagramPostsLabel.text = INSTAGRAM_POSTS_STRING
        instagramFollowersLabel.text = INSTAGRAM_FOLLOWERS_STRING
        instagramFollowingLabel.text = INSTAGRAM_FOLLOWING_STRING
    }

    // MARK: - Convenience
    
    /// Image fetching
    public func setImage(_ url: URL?) {
        if let url = url {
            instagramUserImage.af_setImage(withURL: url, imageTransition: .crossDissolve(0.2), completion: { [weak self] response in
                if let _ = response.result.value {
                    
                } else {
                    self?.instagramUserImage.image = NO_IMAGE
                }
            })
        } else {
            instagramUserImage.image = NO_IMAGE
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        cellDelegate?.logoutButtonPressed()
    }
}
