//
//  AlertController.swift
//  SwiftMediaSharing
//
//  Created by Γιώργος Χαλκιαδούδης on 6/7/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//

import UIKit

protocol AlertPresentable {
    static var alertPresented: Bool { get set }
    
    static func errorAlert(title: String, message: String?, cancelButton: Bool, completion: (() -> Void)?) -> UIAlertController?
}

struct Alert: AlertPresentable {
    static var alertPresented: Bool = false
    
    static func errorAlert(title: String, message: String?, cancelButton: Bool = false, completion: (() -> Void)? = nil) -> UIAlertController? {
        if alertPresented == false {
            alertPresented = true
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let actionOK = UIAlertAction(title: "OK", style: .default) {
                _ in
                guard let completion = completion else { return }
                completion()
            }
            alert.addAction(actionOK)
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            if cancelButton {
                alert.addAction(cancel)
            }
            
            return alert
        } else {
            return nil
        }
    }
}
