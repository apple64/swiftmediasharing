//
//  InstagramUserProfileTableViewController.swift
//  SwiftMediaSharing
//
//  Created by Γιώργος Χαλκιαδούδης on 4/7/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//

import UIKit
import SwiftInstagram

class InstagramUserProfileTableViewController: UITableViewController {
    
    // TableView data source.
    // It contains either an InstagramProfile or InstagramUserPosts
    var dataSource = [Any]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    // MARK: Properties
    
    var userID: String?
    var accessToken: String? {
        didSet {
            // Hide the login button if data is fetched. Show it otherwise.
            if accessToken != nil {
                if let button = self.navigationItem.rightBarButtonItem {
                    button.isEnabled = false
                    button.tintColor = .clear
                }
            } else {
                if let button = self.navigationItem.rightBarButtonItem {
                    button.isEnabled = true
                    button.tintColor = nil
                }
            }
        }
    }
    private var page: UInt = 0
    
    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register tableView cells
        tableView.register(InstagramUserMainProfileTableViewCell.self)
        tableView.register(InstagramPostTableViewCell.self)
        
        // If table view is clear, show a background image
        if dataSource.count > 0 {
            tableView.backgroundView = nil
        } else {
            tableView.backgroundView = UIImageView(image: TABLEVIEW_BACKGROUND)
            tableView.backgroundView?.contentMode = .scaleAspectFit
        }
        
        // Navigation item title view has an Instagram logo
        navigationItem.titleView = UIImageView(image: INSTAGRAM_LOGO)
        
        // Initialise refreshControl for tableView
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action:#selector(refreshData(_:)),
                                  for: UIControlEvents.valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // If there is no data fetched...
        if dataSource.count == 0 {
            // ...read the access token and the user ID
            accessToken = UserDefaults.standard.string(forKey: ACCESS_TOKEN_STRING)
            userID = UserDefaults.standard.string(forKey: USER_ID_STRING)
            
            // If there is an access token and a user ID, fetch the data from Instagram
            if let token = accessToken, token.count > 0 {
                getProfileOf(userID: userID, accessToken: accessToken, page: page, fromService: FetchUserProfileService())
                searchFor(userID: userID, accessToken: accessToken, page: page, fromService: FetchUserProfileService())
            }
        }
    }

    // MARK: - Get Data
    
    /// Fetch user's profile
    ///
    /// - Parameters:
    ///     - userID: User's ID
    ///     - accessToken: Instagram access token
    ///     - page: The result's page to fetch
    ///     - service: A Gettable service
    /// - Returns:
    /// User's Instagram profile in an InstagramProfile object.
    func getProfileOf<Service: Gettable>(userID: String?, accessToken: String?, page: UInt, fromService service: Service) where Service.ProfileData == InstagramProfile? {
        service.getProfileOf(userID: userID, accessToken: accessToken) { [weak self] result in
            switch result {
            case .Success(let result):
                print("Success in fetching the profile")
                // Insert the profile data to the top of the dataSource array
                if let result = result {
                    self?.dataSource.insert(result, at: 0)
                }
            case .Failure(let error):
                print(error)
                // Creating alert
                let alert = Alert.errorAlert(title: "User's profile not fetched", message: error.localizedDescription, cancelButton: false) {
                    Alert.alertPresented = false
                }
                // Present alert
                if let alert = alert {
                    self?.present(alert, animated: true)
                }
            }
        }
    }

    /// Fetch user's recent posts
    ///
    /// - Parameters:
    ///     - userID: User's ID
    ///     - accessToken: Instagram access token
    ///     - page: The result's page to fetch
    ///     - service: A Gettable service
    /// - Returns:
    /// User's Instagram recent posts in an InstagramUserPostsList object.
    func searchFor<Service: Gettable>(userID: String?, accessToken: String?, page: UInt, fromService service: Service) where Service.PostsData == InstagramUserPostsList? {
        service.searchForPostsOf(userID: userID, accessToken: accessToken, page: page) { [weak self] result in
            switch result {
            case .Success(let result):
                print("Success in fetching the posts")
                // Append the recent posts data to the dataSource array
                if let posts = result?.posts {
                    self?.dataSource.append(contentsOf: posts)
                }
            case .Failure(let error):
                print(error)
                // Creating alert
                let alert = Alert.errorAlert(title: "User's recent posts not fetched", message: error.localizedDescription, cancelButton: false) {
                    Alert.alertPresented = false
                }
                // Present alert
                if let alert = alert {
                    self?.present(alert, animated: true)
                }
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func loginBarButtonPressed(_ sender: Any) {
        // Show login view controller
        loginToInstagram()
    }
    
    // MARK: - Convenience
    
    // Login to Instagram
    func loginToInstagram() {
        let api = Instagram.shared
        
        // Show login view controller for Instagram
        api.login(from: navigationController!, success: {
            // Retrieve access token and create user ID
            guard let accessToken = api.retrieveAccessToken() else {
                return
            }
            guard let userID = accessToken.split(separator: ".").first else {
                return
            }
            
            // Save the access token and the user ID
            self.accessToken = accessToken
            self.userID = String(userID)
            
            // Save access token and user ID
            UserDefaults.standard.save(accessToken: accessToken, userID: String(userID))
        }, failure: { [weak self] error in
            print(error)
            // Creating alert
            let alert = Alert.errorAlert(title: "Login to Instagram failed", message: error.localizedDescription, cancelButton: false) {
                Alert.alertPresented = false
            }
            // Present alert
            if let alert = alert {
                self?.present(alert, animated: true)
            }
        })
    }
    
    // Refresh the tableView
    @objc private func refreshData(_ sender: Any) {
        dataSource.removeAll()
        
        // If there is an access token and a user ID, fetch the data from Instagram
        if let token = accessToken, token.count > 0 {
            getProfileOf(userID: userID, accessToken: accessToken, page: page, fromService: FetchUserProfileService())
            searchFor(userID: userID, accessToken: accessToken, page: page, fromService: FetchUserProfileService())
        }
        refreshControl?.endRefreshing()
    }
}

extension InstagramUserProfileTableViewController {
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    // Returns a tableView cell according to the object type.
    // DataSource may have InstagramProfile or InstagramUserPosts.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataSource[indexPath.row] is InstagramProfile {
            let cell: InstagramUserMainProfileTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            let data = dataSource[indexPath.row] as! InstagramProfile
            
            cell.cellDelegate = self
            
            cell.instagramUserFullname.text = data.fullName
            cell.instagramUserWebsite.text = data.website?.absoluteString
            cell.instagramPostsCount.text = "\(data.media ?? 0)"
            cell.instagramFollowersCount.text = "\(data.followedBy ?? 0)"
            cell.instagramFollowingCount.text = "\(data.follows ?? 0)"
            cell.setImage(data.profilePicture) // Fetch the image in real time
            
            return cell
        } else {
            let cell: InstagramPostTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            let data = dataSource[indexPath.row] as! InstagramUserPost
            
            cell.setUserImage(data.user?.profilePicture)
            cell.instagramPostUser.text = data.user?.fullname
            cell.instagramPostLocation.text = data.location?.name
            cell.instagramPostLikes.text = "\(data.likes ?? 0) users liked it"
            if let caption = data.caption, let fromUsername = caption.fromUsername, let text = caption.text  {
                cell.instagramPostComment.text = fromUsername + " " + text
            }
            cell.instagramPostDate.text = data.createdTime
            cell.setImage(data.images?.lowResolution?.url) // Fetch the image in real time
            
            return cell
        }
    }
    
    // Returns the cell height. Simple calculations for demo purposes.
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if dataSource[indexPath.row] is InstagramProfile {
            return INSTAGRAM_USER_MAIN_PROFILE_CELL_HEIGHT
        } else {
            return tableView.frame.width + INSTAGRAM_POST_ADDED_HEIGHT
        }
    }
}

extension InstagramUserProfileTableViewController: InstagramUserMainProfileTableViewCellDelegate {
    func logoutButtonPressed() {
        // Logout from Instagram
        Instagram.shared.logout()
        
        // Save access token and user ID
        UserDefaults.standard.save(accessToken: nil, userID: nil)

        // Clear dataSource, userID and accessToken
        dataSource = [Any]()
        userID = nil
        accessToken = nil

        // Show login view controller
        loginToInstagram()
    }
}
