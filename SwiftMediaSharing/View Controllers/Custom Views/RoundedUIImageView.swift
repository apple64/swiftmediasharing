//
//  RoundedUIImageView.swift
//  SwiftMediaSharing
//
//  Created by Γιώργος Χαλκιαδούδης on 5/7/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  UIImageView with rounded corner

import UIKit

@IBDesignable
class RoundedUIImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    @IBInspectable var borderColor: UIColor = .black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 5.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 5 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    fileprivate func setup() {
        layer.cornerRadius = frame.height / 2
        clipsToBounds = true
    }
}
