//
//  UserDefaults-DataHandling.swift
//  SwiftMediaSharing
//
//  Created by Γιώργος Χαλκιαδούδης on 6/7/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  Save access token and user ID.

import Foundation

extension UserDefaults {
    func save(accessToken: String?, userID: String?) {
        UserDefaults.standard.set(accessToken, forKey: ACCESS_TOKEN_STRING)
        UserDefaults.standard.set(userID, forKey: USER_ID_STRING)
        UserDefaults.standard.synchronize()

        guard let token = accessToken, let id = userID else {
            return
        }
        
        print("------   Loggen in!    ------")
        print("AccessToken \(token)")
        print("User ID \(id)")
        print("-----------------------------")
    }
}
