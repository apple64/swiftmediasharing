//
//  UserDefaults-Contains.swift
//  Tabata & HIIT Seconds Interval
//
//  Created by Γιώργος Χαλκιαδούδης on 5/3/18.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//
//  Checks if UserDefaults contains a specific key

import Foundation

extension UserDefaults {
    func contains(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}
