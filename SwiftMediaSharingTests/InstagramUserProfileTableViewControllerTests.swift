//
//  InstagramUserProfileTableViewController.swift
//  SwiftMediaSharingTests
//
//  Created by George Chalkiadoudis on 06/07/2018.
//  Copyright © 2018 George Chalkiadoudis. All rights reserved.
//

import XCTest
@testable import SwiftMediaSharing

class InstagramUserProfileTableViewControllerTests: XCTestCase {
    
    var viewControllerUnderTest: InstagramUserProfileTableViewController!
    
    let accessToken = UserDefaults.standard.string(forKey: ACCESS_TOKEN_STRING)
    let userID = UserDefaults.standard.string(forKey: USER_ID_STRING)
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        viewControllerUnderTest = storyboard.instantiateViewController(withIdentifier: "InstagramUserProfileTableViewController") as! InstagramUserProfileTableViewController
        
        UIApplication.shared.keyWindow!.rootViewController = viewControllerUnderTest
        
        XCTAssertNotNil(viewControllerUnderTest.view)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testHasATableView() {
        XCTAssertNotNil(viewControllerUnderTest.tableView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(viewControllerUnderTest.tableView.delegate)
    }
    
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(viewControllerUnderTest.conforms(to: UITableViewDelegate.self))
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(viewControllerUnderTest.tableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(viewControllerUnderTest.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:cellForRowAt:))))
    }
    
    func testFetch_FetchProfile() {
        let expectation = XCTestExpectation(description: "Fetch user profile")
        expectation.expectedFulfillmentCount = 1
        
        
        FetchUserProfileService().searchForPostsOf(userID: userID,
                                                   accessToken: accessToken,
                                                   page: 0) { result in
                                                    switch result {
                                                    case .Success(let result):
                                                        XCTAssertNotNil(result?.posts)
                                                        
                                                        expectation.fulfill()
                                                    case .Failure(let error):
                                                        XCTFail(error.localizedDescription)
                                                        
                                                        expectation.fulfill()
                                                    }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFetch_FetchPosts() {
        let expectation = XCTestExpectation(description: "Fetch user posts")
        expectation.expectedFulfillmentCount = 1
        
        
        FetchUserProfileService().getProfileOf(userID: userID,
                                               accessToken: accessToken) { result in
                                                    switch result {
                                                    case .Success(let result):
                                                        XCTAssertNotNil(result?.id)
                                                        
                                                        expectation.fulfill()
                                                    case .Failure(let error):
                                                        XCTFail(error.localizedDescription)
                                                        
                                                        expectation.fulfill()
                                                    }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
}
